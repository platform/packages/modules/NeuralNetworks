// Copyright (C) 2022 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package {
    default_applicable_licenses: ["Android-Apache-2.0"],
}

java_sdk_library {
    name: "framework-ondeviceintelligence-platform",
    srcs: [
        ":framework-ondeviceintelligence-sources-platform",
    ],
    defaults: ["framework-non-updatable-unbundled-defaults"],
    impl_library_visibility: [
        "//frameworks/base:__subpackages__",
    ],
    static_libs: [
        "android.app.ondeviceintelligence-aconfig-java",
    ],
    aidl: {
        include_dirs: [
            "frameworks/base/core/java",
            "frameworks/base/packages/NeuralNetworks/framework/platform/java",
            "frameworks/native/aidl/binder", // For PersistableBundle.aidl
        ],
    },
}

java_sdk_library {
    name: "framework-ondeviceintelligence",
    srcs: [
        ":framework-ondeviceintelligence-sources",
        ":module-utils-future-aidls",
    ],
    defaults: ["framework-module-defaults"],
    sdk_version: "module_current",
    apex_available: [
        "com.android.neuralnetworks",
        "//apex_available:platform",
    ],
    permitted_packages: [
        "android.app.ondeviceintelligence",
        "android.service.ondeviceintelligence",
        "com.android.neuralnetworks.framework.jarjar",
        "com.android.modules.utils",
    ],
    impl_library_visibility: [
        "//packages/modules/NeuralNetworks:__subpackages__",
        "//frameworks/base:__subpackages__",
    ],
    min_sdk_version: "35",
    static_libs: [
        "android.app.ondeviceintelligence-aconfig-java",
        "modules-utils-preconditions",
        "modules-utils-infra",
    ],
    aidl: {
        include_dirs: [
            "frameworks/base/packages/NeuralNetworks/framework/module/java",
        ],
    },
    libs: [
        "unsupportedappusage",
    ],
    jarjar_rules: "jarjar-rules.txt",
}

platform_compat_config {
    name: "framework-ondeviceintelligence-platform-compat-config",
    src: ":framework-ondeviceintelligence-platform",
}
